//exports services
const configuration = require("../controllers/Configuration");


const getTables = (request, response, next) => {
    configuration.getTables().then((res) => {
        response.send(res);
    }).catch((err) => {
        console.log(err);
        response.send({"Error":err});
    }); 
}

const getTableDetail = (request, response, next) => {

    idTableSent = request.query.idTable;
    //validation tree
    if(idTableSent){
        if(!isNaN(idTableSent)){
            if(idTableSent >= 0){
                configuration.getTableDetail(idTableSent).then((res) => {
                    response.send(res);
                }).catch((err) => {
                    console.log(err);
                    esponse.send({"Error":err});
                });            
            }else{
                response.send({"Error":"Parameter idTable must to be >= 0"});
            }
        }else{
            response.send({"Error":"Parameter idTable is not a number"});
        }
    }else{
        response.send({"Error":"Parameter idTable not found"});
    }
}



// export functions to be used over the app
module.exports = {
    getTableDetail,
    getTables
}
