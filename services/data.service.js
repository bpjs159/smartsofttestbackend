//exports services
const data = require("../controllers/Data");
const configuration = require("../controllers/Configuration");

const setNewRecord = (request, response, next) => {
    idTableSent = request.query.idTable;
    isSend = false;

    if(idTableSent){
        if(!isNaN(idTableSent)){
            if(idTableSent >= 0){
                configuration.getTableDetail(idTableSent).then((tableDetail) => {
                    
                    newRecord = Object;

                    switch(idTableSent){
                        case '1':
                            newRecord.table = "TableData1";
                        break;
                        case '2':
                            newRecord.table = "TableData2";
                        break;
                        case '3':
                            newRecord.table = "TableData3";
                        break;
                        default:
                            response.send({"Error":"Table not identified"});
                            isSend = true;
                        break;                        
                    }

                    if(newRecord.table){

                        newRecord.header = [];
                        newRecord.data = [];

                        for(let detail of tableDetail){
                            if(detail.required.lastIndexOf(0) == "-1"){
                                requriedData = true;
                            }else{
                                requriedData = false;
                            }
    
                            if(request.query.hasOwnProperty(detail.Header)){
                                hasData = true;
                            }else{
                                hasData = false;
                            }


                            if(!requriedData || hasData){
                                

                                if(hasData){
                                    if(detail.dataType == 'String'){

                                        //validation data for Sring type
                                                if (typeof request.query[detail.Header] === 'string' || request.query[detail.Header] instanceof String){
                                                    if(request.query[detail.Header].length <= 50){
                                                        newRecord.header.push(detail.Header);
                                                        newRecord.data.push(request.query[detail.Header]);
                                                    }else{
                                                        response.send({"Error":"Parameter "+detail.Header+" must to be a not longer than 50 characters"});
                                                        isSend = true;
                                                        break;
                                                    }
                                                }else{
                                                    response.send({"Error":"Parameter "+detail.Header+" must to be a string"});
                                                    isSend = true;
                                                    break;
                                                }
        
                                    }else if(detail.dataType == 'Int'){
        

                                        //validation data for Int
                                        try {
                                            request.query[detail.Header] = parseInt(request.query[detail.Header])
                                                if (Number.isInteger(request.query[detail.Header])){
                                                    if(request.query[detail.Header].toString().length <= 11){
                                                        newRecord.header.push(detail.Header);
                                                        newRecord.data.push(request.query[detail.Header]);
                                                    }else{
                                                        response.send({"Error":"Parameter "+detail.Header+" must to be a not longer than 11 numbers"});
                                                        isSend = true;
                                                        break;
                                                    }
                                                }else{
                                                    response.send({"Error":"Parameter "+detail.Header+" must to be an integer"});
                                                    isSend = true;
                                                    break;
                                                }
                                        } catch (e) {                                
                                            response.send({"Error":"Parameter "+detail.Header+" must to be an integer: "+ e});
                                            isSend = true;
                                            break;
                                        }
                                        
                                        
                                    }else if(detail.dataType == 'Date'){
        
                                        //validating data for Date type
                                                if (typeof request.query[detail.Header] === 'string' || request.query[detail.Header] instanceof String){
                                                    if(request.query[detail.Header].length <= 150){
                                                        newRecord.header.push(detail.Header);
                                                        newRecord.data.push(request.query[detail.Header]);
                                                    }else{
                                                        response.send({"Error":"Parameter "+detail.Header+" must to be a not longer than 150 numbers"});
                                                        isSend = true;
                                                        break;
                                                    }
                                                }else{
                                                    response.send({"Error":"Parameter "+detail.Header+" must to be an string/date"});
                                                    isSend = true;
                                                    break;
                                                }
                                    }
                                }            
                            


                            }else{
                                response.send({"Error":"Parameter "+detail.Header+" is required"});
                                isSend = true;
                                break;
                            }
                        
                        };
                    }

                    if(!isSend){
                        //request to insert record
                        data.setNewRecord(newRecord).then((res) => {
                            response.send(res);
                        }).catch((err) => {
                            console.log(err);
                            response.send({"Error":err});
                        }); 
                    }
                

                }).catch((err) => {
                    console.log(err);
                    response.send({"Error":err});
                });            
            }else{
                response.send({"Error":"Parameter idTable must to be >= 0"});
            }
        }else{
            response.send({"Error":"Parameter idTable is not a number"});
        }
    }else{
        response.send({"Error":"Parameter idTable not found"});
    }
}

const getTableData = (request, response, next) => {
    idTableSent = request.query.idTable;

    if(idTableSent){
        if(!isNaN(idTableSent)){
            if(idTableSent >= 0){
                switch(idTableSent){
                    case '1':
                        tableName = "TableData1";
                    break;
                    case '2':
                        tableName = "TableData2";
                    break;
                    case '3':
                        tableName = "TableData3";
                    break;
                    default:
                        response.send({"Error":"Table not identified"});
                        isSend = true;
                    break;                        
                }         
                
                if(idTableSent){
                    data.getTableData(tableName).then((res) => {                        
                        response.send(res);
                    }).catch((err) => {
                        console.log(err);
                        response.send({"Error":err});
                 
                    }); 
                }
            }else{
                response.send({"Error":"Parameter idTable must to be >= 0"});
            }
        }else{
            response.send({"Error":"Parameter idTable is not a number"});
        }
    }else{
        response.send({"Error":"Parameter idTable not found"});
    }
}

const updateRecord = (request, response, next) => {
    idTableSent = request.query.idTable;
    isSend = false;

    if(idTableSent){
        if(!isNaN(idTableSent)){
            if(idTableSent >= 0){
                configuration.getTableDetail(idTableSent).then((tableDetail) => {
                    
                    recordUpdate = Object;

                    switch(idTableSent){
                        case '1':
                            recordUpdate.table = "TableData1";
                        break;
                        case '2':
                            recordUpdate.table = "TableData2";
                        break;
                        case '3':
                            recordUpdate.table = "TableData3";
                        break;
                        default:
                            response.send({"Error":"Table not identified"});
                            isSend = true;
                        break;                        
                    }

                    if(recordUpdate.table){

                        recordUpdate.header = [];
                        recordUpdate.data = [];

                        for(let detail of tableDetail){
                            if(detail.required.lastIndexOf(0) == "-1"){
                                requriedData = true;
                            }else{
                                requriedData = false;
                            }
    
                            if(request.query.hasOwnProperty(detail.Header)){
                                hasData = true;
                            }else{
                                hasData = false;
                            }

                                
                                if(hasData){
                                    if(detail.dataType == 'String'){

                                        //validation data for Sring type
                                                if (typeof request.query[detail.Header] === 'string' || request.query[detail.Header] instanceof String){
                                                    if(request.query[detail.Header].length <= 50){
                                                        recordUpdate.header.push(detail.Header);
                                                        recordUpdate.data.push(request.query[detail.Header]);
                                                    }else{
                                                        response.send({"Error":"Parameter "+detail.Header+" must to be a not longer than 50 characters"});
                                                        isSend = true;
                                                        break;
                                                    }
                                                }else{
                                                    response.send({"Error":"Parameter "+detail.Header+" must to be a string"});
                                                    isSend = true;
                                                    break;
                                                }
        
                                    }else if(detail.dataType == 'Int'){
        

                                        //validation data for Int
                                        try {
                                            request.query[detail.Header] = parseInt(request.query[detail.Header])
                                                if (Number.isInteger(request.query[detail.Header])){
                                                    if(request.query[detail.Header].toString().length <= 11){
                                                        recordUpdate.header.push(detail.Header);
                                                        recordUpdate.data.push(request.query[detail.Header]);
                                                    }else{
                                                        response.send({"Error":"Parameter "+detail.Header+" must to be a not longer than 11 numbers"});
                                                        isSend = true;
                                                        break;
                                                    }
                                                }else{
                                                    response.send({"Error":"Parameter "+detail.Header+" must to be an integer"});
                                                    isSend = true;
                                                    break;
                                                }
                                        } catch (e) {                                
                                            response.send({"Error":"Parameter "+detail.Header+" must to be an integer: "+ e});
                                            isSend = true;
                                            break;
                                        }
                                        
                                        
                                    }else if(detail.dataType == 'Date'){
        
                                        //validating data for Date type
                                                if (typeof request.query[detail.Header] === 'string' || request.query[detail.Header] instanceof String){
                                                    if(request.query[detail.Header].length <= 50){
                                                        recordUpdate.header.push(detail.Header);
                                                        recordUpdate.data.push(request.query[detail.Header]);
                                                    }else{
                                                        response.send({"Error":"Parameter "+detail.Header+" must to be a not longer than 50 numbers"});
                                                        isSend = true;
                                                        break;
                                                    }
                                                }else{
                                                    response.send({"Error":"Parameter "+detail.Header+" must to be an string/date"});
                                                    isSend = true;
                                                    break;
                                                }
                                    }
                                }            
                    
                        
                        };
                    }

                    if(!isSend){
                        //request to update record
                        data.updateRecord(recordUpdate).then((res) => {
                            response.send(res);
                        }).catch((err) => {
                            console.log(err);
                            response.send({"Error":err});
                        }); 
                    }
                

                }).catch((err) => {
                    console.log(err);
                    response.send({"Error":err});
                });            
            }else{
                response.send({"Error":"Parameter idTable must to be >= 0"});
            }
        }else{
            response.send({"Error":"Parameter idTable is not a number"});
        }
    }else{
        response.send({"Error":"Parameter idTable not found"});
    }
}

const deleteRecord = (request, response, next) => {
    idTableSent = request.query.idTable;
    isSend = false;

    if(idTableSent){
        if(!isNaN(idTableSent)){
            if(idTableSent >= 0){
                configuration.getTableDetail(idTableSent).then((tableDetail) => {
                    
                    recordDelete = Object;

                    switch(idTableSent){
                        case '1':
                            recordDelete.table = "TableData1";
                            if(request.query.T1C1){
                                recordDelete.header = "T1C1";
                                recordDelete.data = request.query.T1C1;
                            }else{
                                response.send({"Error":"T1C1 is required for delete"});
                                isSend = true;
                            }
                        break;
                        case '2':
                            recordDelete.table = "TableData2";
                            if(request.query.T2C1){
                                recordDelete.header = "T2C1";
                                recordDelete.data = request.query.T2C1;
                            }else{
                                response.send({"Error":"T2C1 is required for delete"});
                                isSend = true;
                            }
                        break;
                        case '3':
                            recordDelete.table = "TableData3";
                            if(request.query.T3C1){
                                recordDelete.header = "T3C1";
                                recordDelete.data = request.query.T3C1;
                            }else{
                                response.send({"Error":"T3C1 is required for delete"});
                                isSend = true;
                            }
                        break;
                        default:
                            response.send({"Error":"Table not identified"});
                            isSend = true;
                        break;                        
                    }

                    if(!isSend){
                        data.deleteRecord(recordDelete).then((res) => {
                            response.send(res);
                        }).catch((err) => {
                            console.log(err);
                            response.send({"Error":err});
                        }); 
                    }
                

                }).catch((err) => {
                    console.log(err);
                    response.send({"Error":err});
                });            
            }else{
                response.send({"Error":"Parameter idTable must to be >= 0"});
            }
        }else{
            response.send({"Error":"Parameter idTable is not a number"});
        }
    }else{
        response.send({"Error":"Parameter idTable not found"});
    }
}


// export functions to be used over the app
module.exports = {
    setNewRecord,
    getTableData,
    updateRecord,
    deleteRecord
}
