//const declaration of express and router
const express = require('express');
const router = express.Router();

//import services to use
const configurationService = require("./services/configuration.service");
const dataService = require("./services/data.service");

//declare services
//for configuration

router.get("/getTableDetail",configurationService.getTableDetail);
router.get("/getTables",configurationService.getTables);

//for data
router.get("/setNewRecord",dataService.setNewRecord);
router.get("/getTableData",dataService.getTableData);
router.get("/updateRecord",dataService.updateRecord);
router.get("/deleteRecord",dataService.deleteRecord);





module.exports = router;