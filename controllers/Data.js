//import database query
const setNewRecordQuery = require("../dataaccess/queries/setNewRecord");
const getTableDataQuery = require("../dataaccess/queries/getTableData");
const updateRecordQuery = require("../dataaccess/queries/updateRecord");
const deleteRecordQuery = require("../dataaccess/queries/deleteRecord");


//declare controllers functions
const setNewRecord = (newRecord) => {
    return setNewRecordQuery.setNewRecord(newRecord);
}

const getTableData = (tableName) => {
    return getTableDataQuery.getTableData(tableName);
}

const updateRecord = (recordUpdate) => {
    return updateRecordQuery.updateRecord(recordUpdate);
}

const deleteRecord = (recordDelete) => {
    return deleteRecordQuery.deleteRecord(recordDelete);
}


// export functions to be used over the app
module.exports = {
    deleteRecord,
    getTableData,
    setNewRecord,
    updateRecord
}