//import database query
const getTablesQuery = require("../dataaccess/queries/getTables");
const getTableDetailQuery = require("../dataaccess/queries/getTableDetail");

//declare controllers functions
const getTableDetail = (idTable) => {
    return getTableDetailQuery.getTableDetail(idTable);
}

const getTables = () => {
    return getTablesQuery.getTables();
}

// export functions to be used over the app
module.exports = {
    getTableDetail,
    getTables
}
