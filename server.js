//declare express, app and port const
const express = require("express");
const API = require("./API");
const app = express();
const port = 3000;

//add cors to headers
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();

    app.options('*', (req, res) => {
        res.header('Access-Control-Allow-Methods', 'GET, PATCH, PUT, POST, DELETE, OPTIONS');
        res.send();
    });
});

//case of access root
app.get("/",function(req,res){
    res.send("Hello!, this is Smartsoft test API");
});

//attatch routing with API
app.use("/api", API);

//launch app on port 
app.listen(port,() => {
    console.log("Smartsoft test backend running on port "+port);
})