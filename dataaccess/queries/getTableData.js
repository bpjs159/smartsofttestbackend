//get maridb 
const mariadbJS = require("../mariadb");

//function getTableData
exports.getTableData = function (tableName) {
    firstCol:string = "";

    return mariadbJS.mariadbquery("SHOW COLUMNS FROM "+tableName).then((cols) => {        
        firstCol = cols[0].Field;
        return mariadbJS.mariadbquery("SELECT * FROM "+tableName+" ORDER BY "+firstCol+" DESC ");   
    });

     
}