//get maridb 
const mariadbJS = require("../mariadb");

//function deleteRecord
const deleteRecord = function (recordDelete) {
    return mariadbJS.mariadbquery("DELETE FROM "+recordDelete.table+" WHERE "+recordDelete.header+" = "+recordDelete.data);    
}


// export functions to be used over the app
module.exports = {
    deleteRecord
}