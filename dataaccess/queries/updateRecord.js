//get maridb 
const mariadbJS = require("../mariadb");

//function updateRecord
const updateRecord = function (recordUpdate) {

    queryString = "UPDATE "+recordUpdate.table+" SET ";
                i = 0; 
                for(let head of recordUpdate.header){
                    queryString += head+" = "+"'"+recordUpdate.data[i]+"'";
                    i++;                        

                    if(i != recordUpdate.header.length ){
                        queryString += ", ";
                    }
                }

                queryString += " WHERE "+recordUpdate.header[0]+" = "+recordUpdate.data[0];

    return mariadbJS.mariadbquery(queryString);
}


// export functions to be used over the app
module.exports = {
    updateRecord
}