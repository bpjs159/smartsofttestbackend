//get maridb 
const mariadbJS = require("../mariadb");

//function setNewRecord
const setNewRecord = function (newRecord) {
    queryRecord = "INSERT INTO "+newRecord.table+" ("+newRecord.header.join()+") VALUES ('"+newRecord.data.join("','")+"')";
    return mariadbJS.mariadbquery(queryRecord);      
}


// export functions to be used over the app
module.exports = {
    setNewRecord
}