//declare mariadb package and json credentials
const mariadb = require('mariadb');
credentials = require("./credentials.json");

//create mariadb

const mariadbquery = (query) => {
    return mariadb.createConnection(credentials)
    .then(conn => {
      return conn.query(query)
        .then(response => {
            //close connection
            conn.end();     
            return response;        
        })
        .catch(err => { 
          //handle query error
          conn.end();
          return { "Error": err.errno+ ": " + err.code }
        });
    })
    .catch(err => {      
        conn.end();
        return { "Error": err.errno+ ": " + err.code }
    });
}


module.exports = {
    mariadbquery
}